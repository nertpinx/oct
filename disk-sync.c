/*
 * Open Conversion Tools
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include "config.h"

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <inttypes.h>
#include <libnbd.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define NBD_STATE_HOLE (1 << 0)
#define NBD_STATE_ZERO (1 << 1)

#define NBD_WARN(fmt, ...) \
    warnx(fmt ": %s (errno = %d)\n", \
          __VA_ARGS__, nbd_get_error(), nbd_get_errno())

#define NBD_ERROR(fmt, ...) \
    do { \
        NBD_WARN(fmt, __VA_ARGS__); \
        goto error; \
    } while (0)

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX_REQ_SIZE_BLOCK_STATUS (2ULL << 30)
#define MAX_REQ_SIZE_PREAD (64ULL << 20)

#define DBG(fmt, ...) \
    do { \
        if (verbose) \
            printf("%s: debug: " fmt "\n", progname, __VA_ARGS__); \
    } while (0)

struct extents {
    struct extent {
        uint64_t offset;
        uint64_t length;
        uint32_t flags;
    } *extents;
    uint64_t alloc_extents;
    uint64_t nr_extents;
};

static bool verbose;
static bool debug;
static bool show_progress;
static bool nice_progress;
static const char *progname;
static const char *nbd_uri;
static const char *local_file;
static bool single_layer;
static const char *nbd_uri_single_layer;
static size_t max_read_bytes;

static void
usage(void)
{
    printf("%s: synchronize remote disk state to local file\n"
           "\n"
           "Usage:\n"
           "    %s [-s [NBD_URI]] [-v] [-x] <nbd_uri> <local_file>\n"
           "    %s [-V | -h ]\n"
           "\n"
           "Arguments:\n"
           "    nbd_uri       NBD URI to connect to\n"
           "    local_file    Path to local file to sync to\n"
           "\n"
           "Options:\n"
           "    --megs=COUNT          Read maximum COUNT MB at a time\n"
           "    -p | --progress       Show progress even if stdout is not a tty\n"
           "    -n | --no-progress    Do not show progress\n"
           "    --single[=NBD_URI]    Single layer sync only (for layered sync\n"
           "                          instead of full disk), the NBD_URI is an\n"
           "                          (optional) URI to use for extent mapping\n"
           "    -v | --verbose        Be more verbose (changes progress format)\n"
           "    -V | --version        Print version and exit\n"
           "    -x | --debug          Print debugging info (implies -v)\n"
           "    -h | --help           Print this help and exit\n"
           "\n"
           ,//           "For detailed help please read the man page oct-disk-sync(1).\n",
           progname, progname, progname);
}

static int
get_number(const char *str,
           unsigned long long *value,
           int base)
{
    char *endptr = NULL;

    errno = 0;
    *value = strtoull(str, &endptr, base);

    if (errno || endptr == optarg || *endptr != '\0') {
        if (errno == 0)
            errno = EINVAL;
        return -1;
    }

    return 0;
}

static void
progress_clear(void)
{
    int cols = 80;
    char *str = getenv("COLUMNS");

    if (!nice_progress)
        return;

    if (str) {
        unsigned long long tmp = 0;
        if (get_number(str, &tmp, 10) > 0 &&
            tmp < INT_MAX)
            cols = tmp;
    }

    printf("\r");
    for (int i = 0; i < cols; i++)
        printf(" ");
    printf("\r");
}

static void
progress_failed(void)
{
    if (nice_progress)
        printf(" --- Failed\n");
}

static void __attribute__ ((format (printf, 3, 4)))
progress_update(uint64_t done, uint64_t total, const char *fmt, ...)
{
    va_list ap;

    if (!show_progress)
        return;

    progress_clear();

    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);

    printf(": %d%%", (int) (done / (total / 100)));
    if (!nice_progress)
        printf("\n");
    fflush(stdout);
}

static void __attribute__ ((format (printf, 1, 2)))
progress_done(const char *fmt, ...)
{
    va_list ap;

    if (!show_progress)
        return;

    progress_clear();

    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);

    printf(": Done\n");
    fflush(stdout);
}


static int
block_status_cb(void *opaque,
                const char *metacontext,
                uint64_t offset,
                uint32_t *entries,
                size_t nr_entries,
                int *error)
{
    struct extents *e = opaque;
    size_t last_offset = offset;
    uint64_t required_extents = e->nr_extents + nr_entries/2;

    DBG("block_status_cb("
        "opaque=%p, "
        "metacontext='%s', "
        "offset=%" PRIu64 ", "
        "entries=%p, "
        "nr_entries=%zu, "
        "error=%p, *error=%d)",
        opaque, metacontext, offset, entries, nr_entries, error, *error);

    if (strcmp("base:allocation", metacontext) != 0) {
        DBG("%s", "Skipping uninteresting extents");
        return 0;
    }

    if (e->alloc_extents < required_extents) {
        void *tmp = realloc(e->extents,
                            sizeof(struct extent) * required_extents);
        if (!tmp) {
            warn("Failed to reallocate extents array");
            return -1;
        }
        e->extents = tmp;
    }

    if (nr_entries % 2) {
        warnx("Odd number of replies for base:allocation");
        return -1;
    }

    for (size_t i = 0; i < nr_entries; i += 2) {
        struct extent *ei = e->extents + e->nr_extents;

        DBG("adding entry into the list #%zu: %" PRIu32 " %" PRIu32,
            i, entries[i], entries[i + 1]);

        ei->offset = last_offset;
        ei->length = entries[i];
        ei->flags = entries[i + 1];
        e->nr_extents++;

        last_offset += entries[i];
    }

    return 0;
}

static int
disk_sync(int fd)
{
    struct nbd_handle *nbd = nbd_create();
    struct nbd_handle *nbd_single = nbd;
    struct extents e = {0};
    int64_t size = 0;
    uint64_t size_aligned = 0;
    uint64_t last_offset = 0;
    int ret = -1;
    long page_size = sysconf(_SC_PAGE_SIZE);
    uint8_t *filebuf = NULL;
    bool connected = false;
    bool connected_single = false;
    uint64_t transfer_needed = 0;
    uint64_t transfer_skipped = 0;
    uint64_t total_bytes_read = 0;

    if (!nbd)
        NBD_ERROR("%s", "Failed to create nbd_handle");

    nbd_set_debug(nbd, debug);

    if (nbd_uri_single_layer) {
        nbd_single = nbd_create();

        if (!nbd_single)
            NBD_ERROR("%s", "Failed to create second nbd_handle");

        nbd_set_debug(nbd_single, debug);
    }

    if (nbd_add_meta_context(nbd_single, "base:allocation") == -1)
        NBD_ERROR("%s", "Failed to add 'base:allocation' meta context");

    if (nbd_connect_uri(nbd, nbd_uri) == -1)
        NBD_ERROR("Could not connect to '%s'", nbd_uri);

    connected = true;

    DBG("NBD_URI_SINGLE_LAYER=%s", nbd_uri_single_layer ?: "");
    if (nbd_uri_single_layer) {
        if (nbd_connect_uri(nbd_single, nbd_uri_single_layer) == -1)
            NBD_ERROR("Could not connect to '%s'", nbd_uri);
    }

    connected_single = true;

    size = nbd_get_size(nbd);
    if (size < 0)
        NBD_ERROR("%s", "Failed to get size of the export");

    DBG("Size of the disk is %" PRIi64 " Bytes", size);
    size_aligned = ((size + page_size - 1) / page_size) * page_size;
    DBG("Aligned size is %" PRIu64 " Bytes", size_aligned);

    progress_update(0, size, "Getting allocation information");

    while (last_offset < (uint64_t) size) {
        struct extent *last_extent = NULL;
        uint64_t length = MIN(size - last_offset, MAX_REQ_SIZE_BLOCK_STATUS);

        DBG("Requesting block_status "
            "from %" PRIu64 " to %" PRIu64 "(%" PRIu64 ")",
            last_offset, last_offset + length, length);
        if (nbd_block_status(nbd_single,
                             length, last_offset,
                             &e, block_status_cb, 0) == -1) {
            progress_failed();
            NBD_ERROR("%s", "Failed to get block_status");
        }

        if (e.nr_extents == 0) {
            progress_failed();
            warnx("No block status data received");
            goto error;
        }

        last_extent = e.extents + e.nr_extents - 1;

        if (last_offset >= last_extent->offset + last_extent->length) {
            progress_failed();
            warnx("No block status data were updated");
            goto error;
        }

        last_offset = last_extent->offset + last_extent->length;
        if (last_offset > (uint64_t) size) {
            progress_failed();
            warnx("Block status is describing more bytes than "
                  "there are in the file: "
                  "last_offset = %" PRIu64 ", size = %" PRIi64,
                  last_offset, size);
            goto error;
        }

        progress_update(last_offset, size,
                        "Getting allocation information");
    }

    progress_done("Getting allocation information");

    for (size_t i = 0; i < e.nr_extents; i++) {
        struct extent *ei = e.extents + i;

        if (ei->flags & (NBD_STATE_ZERO | NBD_STATE_HOLE))
            transfer_skipped += ei->length;
        else
            transfer_needed += ei->length;
    }

    DBG("Data: transferring %" PRIu64 " B; Skipping %" PRIu64 " B\n",
        transfer_needed, transfer_skipped);

    DBG("Total aligned size of data: %" PRIu64 " B", size_aligned);
    DBG("Total size of data:         %" PRIu64 " B", size);
    DBG("Described by block status:  %" PRIu64 " B",
        transfer_skipped + transfer_needed);

    if ((uint64_t) size != transfer_skipped + transfer_needed)
        warnx("Described size is not equal to total size");

    if (ftruncate(fd, size_aligned) < 0)
        warn("Error when running ftruncate to align the file size");

    filebuf = mmap(NULL, size_aligned, PROT_WRITE, MAP_SHARED, fd, 0);
    if (filebuf == MAP_FAILED) {
        filebuf = NULL;
        warn("Failed to mmap the local file");
        goto error;
    }

    progress_update(0, size, "Transferring data");

    for (size_t i = 0; i < e.nr_extents; i++) {
        struct extent *ei = e.extents + i;

        DBG("processing extent #%zu: 0x%06" PRIx64 "-0x%06" PRIx64 ": %" PRIx32,
            i, ei->offset, ei->offset + ei->length, ei->flags);

        if (ei->flags & NBD_STATE_HOLE && single_layer)
            continue;

        if (ei->flags & (NBD_STATE_ZERO | NBD_STATE_HOLE)) {
            DBG("Punching out %" PRIu64 " B at offset %" PRIu64,
                ei->length, ei->offset);
            if (fallocate(fd,
                          FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE,
                          ei->offset, ei->length) < 0) {
                DBG("%s", "Punching hole failed, falling back to manual");
                if (single_layer) {
                    DBG("%s", "Using memset instead of punching hole");
                    memset(filebuf + ei->offset, 0, ei->length);
                }
            }
        } else {
            uint64_t bytes_read = 0;

            DBG("Transferring %" PRIu64 " B from offset %" PRIu64,
                ei->length, ei->offset);

            while (bytes_read < ei->length) {
                uint64_t offset = ei->offset + bytes_read;
                size_t to_read = MIN(ei->length - bytes_read,
                                     max_read_bytes);

                DBG("pread of %" PRIu64 " B from offset %" PRIu64,
                    to_read, offset);
                if (nbd_pread(nbd, filebuf + offset, to_read, offset, 0) == -1) {
                    progress_failed();
                    NBD_ERROR("%s", "Failed to read from file");
                }

                bytes_read += to_read;

                total_bytes_read += to_read;
                progress_update(total_bytes_read, transfer_needed,
                                "Transferring data");
            }
        }
    }

    progress_done("Transferring data");

    ret = 0;
 error:
    if (ftruncate(fd, size) < 0)
        warn("Error when running ftruncate to get back to proper size");
    close(fd);
    free(e.extents);

    if (filebuf && munmap(filebuf, size_aligned) < 0)
        warn("Error while trying to munmap the local file");

    if (nbd_uri_single_layer) {
        if (connected_single &&
            nbd_shutdown(nbd_single) == -1)
            NBD_WARN("%s", "Error shutting down the NBD connection");
        nbd_close(nbd_single);
    }

    if (connected &&
        nbd_shutdown(nbd) == -1)
        NBD_WARN("%s", "Error shutting down the NBD connection");

    nbd_close(nbd);
    return ret;
}

int
main(int argc, char **argv)
{
    const char options[] = "hm:npvVx";
    const struct option long_options[] = {
                                          { "help", 0, NULL, 'h' },
                                          { "megs", 1, NULL, 'm' },
                                          { "no-progress", 0, NULL, 'n' },
                                          { "progress", 0, NULL, 'p' },
                                          { "single", 2, NULL, 's' },
                                          { "verbose", 0, NULL, 'v' },
                                          { "version", 0, NULL, 'V' },
                                          { "debug", 0, NULL, 'x' },
                                          { 0 }
    };

    progname = basename(argv[0]);
    show_progress = nice_progress = isatty(STDOUT_FILENO);

    while (1) {
        int c = getopt_long(argc, argv, options, long_options, NULL);
        if (c == -1)
            break;

        switch (c) {
        case 'm':
            {
                unsigned long long tmp;
                if (get_number(optarg, &tmp, 0) < 0)
                    err(EXIT_FAILURE,
                        "Failed to parse bytes to transfer: '%s'", optarg);

                if (tmp > MAX_REQ_SIZE_PREAD >> 20) {
                    errno = EINVAL;
                    err(EXIT_FAILURE,
                        "Value %llu too big, max is %llu",
                        tmp, MAX_REQ_SIZE_PREAD >> 20);
                }

                max_read_bytes = tmp << 20;
            }
            break;

        case 'h':
            usage();
            exit(EXIT_FAILURE);
            break;

        case 'n':
            show_progress = false;
            break;

        case 'p':
            show_progress = true;
            break;

        case 's':
            DBG("Enabling single-layer, uri='%s'", optarg);
            single_layer = true;
            nbd_uri_single_layer = optarg;
            break;

        case 'v':
            verbose = true;
            break;

        case 'V':
            printf("%s %s\n", progname, PACKAGE_VERSION);
            exit(EXIT_SUCCESS);
            break;

        case 'x':
            debug = verbose = true;
            break;

        default:
            exit(EXIT_FAILURE);
        }
    }

    if (verbose)
        nice_progress = false;

    DBG("Max bytes to read (supplied): %zu", max_read_bytes);
    if (!max_read_bytes)
        max_read_bytes = MAX_REQ_SIZE_PREAD;
    DBG("Max bytes to read (actual): %zu", max_read_bytes);

    assert(optind <= argc);

    if (argc == optind)
        errx(EXIT_FAILURE,
             "Both mandatory arguments missing (nbd_uri and local_file)");

    if (argc == optind + 1)
        errx(EXIT_FAILURE,
             "Second mandatory argument missing (local_file)");

    if (argc > optind + 2)
        errx(EXIT_FAILURE,
             "Extra arguments while only two are needed");

    nbd_uri = argv[optind++];
    local_file = argv[optind];

    unsigned int flags = O_RDWR; // O_WRONLY is enough if we don't do mmap(2)
    if (!single_layer)
        flags |=  O_CREAT;

    int fd = open(local_file, flags, S_IRUSR | S_IWUSR);
    if (fd < 0) {
        if (single_layer) {
            err(EXIT_FAILURE,
                "Cannot write-open local file '%s'",
                local_file);
        }
        err(EXIT_FAILURE,
            "Cannot exclusively create file '%s'",
            local_file);
    }

    if (disk_sync(fd) < 0) {
        close(fd);
        if (!single_layer) {
            unlink(local_file);
        }

        return EXIT_FAILURE;
    }

    close(fd);
    return EXIT_SUCCESS;
}
