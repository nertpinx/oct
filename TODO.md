### There are still a lot of missing things to even get started properly:

- Machine readable status reporting
- Something to start the nbdkit processes
- Something to connect to a remote hypervisor using libvirt, parse the XML, do a
  snapshot and run disk-sync for all the disks
- Tests

### And some "nice-to-have"s:

- CI/CD with artifacts for releases
- Some webpage (part of GitLab)
