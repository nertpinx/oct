# Open Conversion Tools

Collection of tools helping with various non-standard VM conversions.

Very early stage of development.

# Related projects

- [virt-v2v](http://www.libguestfs.org/virt-v2v.1.html) as part of [libguestfs](http://www.libguestfs.org)
- [nbdkit](https://github.com/libguestfs/nbdkit)
- [v2v-wrapper](https://github.com/oVirt/v2v-conversion-host/blob/master/docs/Virt-v2v-wrapper.md)
